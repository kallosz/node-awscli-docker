FROM node:14.5-alpine3.12

ENV PACKAGES "tree python3 py-pip yarn"

ARG tag=latest
ARG build=latest

ENV TAG=$tag \
    BUILD=$build

RUN apk add --no-cache $PACKAGES \
    && pip3 install awscli
